package com.example.meg.photoshareapp.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.utils.FontManager;

public class CreateGroupActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mGroupName, mGroupDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        initToolbar();
        initViews();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setTitle(getResources().getString(R.string.create_group));
    }

    private void initViews() {
        Typeface iconFont = FontManager.getTypeface(ApplicationLoader.getInstance(), FontManager.MATERIALICON);
        mGroupName = findViewById(R.id.etGroupName);
        mGroupDescription = findViewById(R.id.etGroupDescription);
        TextView tvCancelGroup = findViewById(R.id.tvCancelGroup);
        tvCancelGroup.setTypeface(iconFont);
        tvCancelGroup.setOnClickListener(this);
        TextView tvCreateGroup = findViewById(R.id.tvCreateGroup);
        tvCreateGroup.setTypeface(iconFont);
        tvCreateGroup.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvCreateGroup:
                if (validate()) {
                    String name = mGroupName.getText().toString().trim();
                    String description = mGroupDescription.getText().toString().trim();

                    Intent intent = new Intent(CreateGroupActivity.this, AddMembersActivity.class);
                    intent.putExtra("groupName", name);
                    intent.putExtra("groupDescription", description);
                    startActivity(intent);
                }
                break;
            case R.id.tvCancelGroup:
                finish();
                break;
        }

    }

    private boolean validate() {
        if (mGroupName.getText().toString().trim().length() == 0) {
            Toast.makeText(this, "Please Enter Group Name", Toast.LENGTH_SHORT).show();
            return false;
        } else return true;
    }
}
