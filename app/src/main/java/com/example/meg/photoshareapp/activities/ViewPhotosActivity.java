package com.example.meg.photoshareapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.adapters.ImagesAdapter;
import com.example.meg.photoshareapp.adapters.SectionRecyclerAdapter;
import com.example.meg.photoshareapp.models.Group;
import com.example.meg.photoshareapp.models.Image;
import com.example.meg.photoshareapp.models.SectionModel;
import com.example.meg.photoshareapp.utils.RecyclerViewType;
import com.example.meg.photoshareapp.utils.Utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.example.meg.photoshareapp.utils.RecyclerViewType.GRID;

public class ViewPhotosActivity extends AppCompatActivity {
    private String mLocation;
    private RelativeLayout mProgressBar;
    private Group mGroup = new Group();

    private RecyclerView mimageRecyclerView;
    private ImagesAdapter mImagesAdapter;
    private List<Image> imageList = new ArrayList<>();
    private TextView mtvNoImageText;

    private RecyclerViewType recyclerViewType = GRID;
    private SectionRecyclerAdapter mAdater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_photos);
        getBundleData();
        initToolbar();
        initViews();
    }

    private void getBundleData() {
        if (getIntent().getExtras() != null) {
            mLocation = getIntent().getStringExtra("location");
            mGroup = (Group) getIntent().getSerializableExtra("group");
        }
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setTitle(mLocation);
    }

    private void initViews() {
        TextView addImage = findViewById(R.id.addImage);
        addImage.setVisibility(View.GONE);
        mProgressBar = findViewById(R.id.progressBarLayout);


        mimageRecyclerView = findViewById(R.id.imageRecyclerView);
        mImagesAdapter = new ImagesAdapter(this);
        mimageRecyclerView.setHasFixedSize(true);
        LinearLayoutManager recentTransactionLinearLayoutManager = new LinearLayoutManager(ApplicationLoader.getInstance());
        recentTransactionLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mimageRecyclerView.setLayoutManager(recentTransactionLinearLayoutManager);
        mimageRecyclerView.setAdapter(mAdater);
        mtvNoImageText = findViewById(R.id.tvNoImageText);
        mtvNoImageText.setVisibility(View.GONE);
        mimageRecyclerView.setVisibility(View.VISIBLE);
        setData();

    }

    private List<Image> getImagesToShow() {
        List<Image> imageUrl = new ArrayList<>();

        for (int i = 0; i < mGroup.getImages().size(); i++) {
            if (mGroup.getImages().get(i).getLocation().equals(mLocation)) {
                imageUrl.add(mGroup.getImages().get(i));
            }
        }

        return imageUrl;
    }


    private void setData() {
        if (imageList != null) {
            mtvNoImageText.setVisibility(View.GONE);
            mimageRecyclerView.setVisibility(View.VISIBLE);

            if (Utils.getSortedDateAndImageList(getImagesToShow()) != null) {
                LinkedHashMap<String, List<Image>> map = Utils.getSortedDateAndImageList(getImagesToShow());

                if (map != null) {
                    ArrayList<SectionModel> sectionModelArrayList = new ArrayList<>();
                    List<String> keys = new ArrayList<>();
                    List<Image> value = new ArrayList<>();
                    for (Map.Entry<String, List<Image>> ee : map.entrySet()) {
                        String key = ee.getKey();
                        List<Image> values = ee.getValue();
                        sectionModelArrayList.add(new SectionModel(ee.getKey(), ee.getValue()));
                        keys.add(key);
                        for (int i = 0; i < ee.getValue().size(); i++) {
                            value.add(values.get(i));
                        }
                    }

                    mAdater = new SectionRecyclerAdapter(ApplicationLoader.getInstance(), recyclerViewType, sectionModelArrayList);
                    mimageRecyclerView.setAdapter(mAdater);
                }
            }
        }
    }


}
