package com.example.meg.photoshareapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.activities.ViewPhotosActivity;
import com.example.meg.photoshareapp.models.Group;
import com.example.meg.photoshareapp.models.Image;

import java.util.ArrayList;
import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {

    private Context mContext;
    private List<String> mLocationList;
    private Group mGroup;

    public LocationAdapter(Context context, Group mGroup) {
        mContext = context;
        mLocationList = new ArrayList<>();
        this.mGroup = mGroup;
    }

    public void setDataInAdapter(List<String> location) {
        mLocationList = location;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_add_member, parent, false);
        return new LocationAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tvCancel.setText(ApplicationLoader.getInstance().getResources().getString(R.string.view_photos));

        if (mLocationList != null) {
            holder.tvMemberSelected.setText(mLocationList.get(position));

            holder.tvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ApplicationLoader.getInstance(), ViewPhotosActivity.class);
                    intent.putExtra("location", mLocationList.get(position));
                    intent.putExtra("group", mGroup);
                    ApplicationLoader.getInstance().startActivity(intent);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mLocationList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView tvCancel, tvMemberSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCancel = itemView.findViewById(R.id.tvCancel);
            tvMemberSelected = itemView.findViewById(R.id.tvMemberSelected);
            mView = itemView;
        }

    }
}
