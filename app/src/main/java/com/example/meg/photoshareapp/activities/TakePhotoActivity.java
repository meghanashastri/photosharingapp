package com.example.meg.photoshareapp.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.models.Image;
import com.example.meg.photoshareapp.utils.CameraActivity;
import com.example.meg.photoshareapp.utils.Constants;
import com.example.meg.photoshareapp.utils.FontManager;
import com.example.meg.photoshareapp.utils.ImageUtility;
import com.example.meg.photoshareapp.utils.SaveImageDetails;
import com.example.meg.photoshareapp.utils.SharedPreferenceManager;
import com.example.meg.photoshareapp.utils.Utils;
import com.firebase.client.Firebase;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import com.example.meg.photoshareapp.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;


public class TakePhotoActivity extends AppCompatActivity implements View.OnClickListener {
    Intent intent;
    private ImageView mImageView;
    private FusedLocationProviderClient mFusedLocationClient;
    private String mLocation, mTimestamp;
    private RelativeLayout mSelectLayout;
    private LinearLayout mDescriptionLayout;
    private TextView mtvTimestampAndLocation;
    private EditText mEtTitle, mEtDescription;
    private Bitmap bitmap;
    private Firebase mFirebaseRef;
    private static final String FIREBASE_URL = "https://photoshareapp-2368d.firebaseio.com/";
    private String mChannel = UUID.randomUUID().toString();
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef;
    private RelativeLayout mProgressBar, mshareLayout;
    private String mUrl;

    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private Point mSize;
    Uri photoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_take_photo);
        initToolbar();
        initViews();
        mFirebaseRef = new Firebase(FIREBASE_URL).child(mChannel);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        enableRuntimePermission();

        Display display = getWindowManager().getDefaultDisplay();
        mSize = new Point();
        display.getSize(mSize);

    }

    private void launch() {
        Intent startCustomCameraIntent = new Intent(this, CameraActivity.class);
        startActivityForResult(startCustomCameraIntent, REQUEST_CAMERA);
    }


    private void initViews() {
        Typeface iconFont = FontManager.getTypeface(ApplicationLoader.getInstance(), FontManager.MATERIALICON);
        mProgressBar = findViewById(R.id.progressBarLayout);
        mImageView = findViewById(R.id.imageView);
        TextView tvSelect = findViewById(R.id.tvSelect);
        tvSelect.setTypeface(iconFont);
        tvSelect.setOnClickListener(this);
        TextView tvCancel = findViewById(R.id.tvCancel);
        tvCancel.setTypeface(iconFont);
        tvCancel.setOnClickListener(this);
        mSelectLayout = findViewById(R.id.selectLayout);
        mDescriptionLayout = findViewById(R.id.descriptionLayout);
        mtvTimestampAndLocation = findViewById(R.id.tvTimestampAndLocation);
        mEtTitle = findViewById(R.id.etTitle);
        mEtDescription = findViewById(R.id.etDescription);
        TextView tvShare = findViewById(R.id.tvShare);
        tvShare.setTypeface(iconFont);
        tvShare.setOnClickListener(this);
        TextView tvSkip = findViewById(R.id.tvSkip);
        tvSkip.setOnClickListener(this);
        mshareLayout = findViewById(R.id.shareLayout);

    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setTitle("Take Photo");
    }

    public void enableRuntimePermission() {

        if (Build.VERSION.SDK_INT >= 23 && checkSelfPermission(Manifest.permission.
                CAMERA) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.CAMERA) !=
                PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.
                ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ArrayList<String> permissions = new ArrayList<>();
            permissions.add(Manifest.permission.CAMERA);
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            String[] items = permissions.toArray(new String[permissions.size()]);
            requestPermissions(items, REQUEST_CAMERA);
        } else {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                mLocation = getCompleteAddressString(location.getLatitude(), location.getLongitude());
                            }
                        }
                    });

            launch();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {

           /* bitmap = (Bitmap) data.getExtras().get("data");

            mImageView.setImageBitmap(bitmap);*/

             photoUri = data.getData();
            // Get the bitmap in according to the width of the device
            bitmap = ImageUtility.decodeSampledBitmapFromPath(photoUri.getPath(), mSize.x, mSize.x);
            mImageView.setImageBitmap(bitmap);

            mTimestamp = Utils.getTimestamp();
            Log.d("timestamp", mTimestamp);
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {

        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:

                // If request is cancelled, the result arrays are empty.
                boolean phoneStateAccecpted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                boolean fineLocationAccecpted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                boolean coarseLocationAccecpted = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                boolean writeStorage = grantResults[3] == PackageManager.PERMISSION_GRANTED;

                boolean readStorage = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                ;

                if (fineLocationAccecpted && coarseLocationAccecpted) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    mFusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        mLocation = getCompleteAddressString(location.getLatitude(), location.getLongitude());
                                    }
                                }
                            });
                }
                if (phoneStateAccecpted && writeStorage && readStorage) {
                    launch();
                }


                break;
        }
    }

    private String getCompleteAddressString(double latitude, double longitude) {
        String location = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                location = strReturnedAddress.toString();
                Log.w("My Currentaddress", strReturnedAddress.toString());
            } else {
                Log.w("Myaddress", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("address", "Canont get Address!");
        }
        return location;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mLocation != null && mTimestamp != null) {
            mtvTimestampAndLocation.setText(mTimestamp + ". " + mLocation);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvSelect:
                if (validateTitle()) {
                    if (bitmap != null) {
                        // CALL THIS METHOD TO GET THE ACTUAL PATH
                        String realPath = getRealPathFromURI(photoUri);
                        if (realPath != null) {
                            sendDataToImageStorage(realPath, view);
                        }
                    }

                    mSelectLayout.setVisibility(View.GONE);
                    mshareLayout.setVisibility(View.VISIBLE);
                    mEtTitle.setEnabled(false);
                    mEtDescription.setEnabled(false);
                } else {
                    mshareLayout.setVisibility(View.GONE);
                    mSelectLayout.setVisibility(View.VISIBLE);
                    mEtTitle.setEnabled(true);
                    mEtDescription.setEnabled(true);
                }
                break;
            case R.id.tvCancel:
                finish();
                break;
            case R.id.tvShare:
                Intent intent = new Intent(ApplicationLoader.getInstance(), HomeActivity.class);
                intent.putExtra(Constants.FLAG, Constants.TAKE_PHOTO_ACTIVITY_FLAG);
                intent.putExtra("position", 1);
                intent.putExtra("location", mLocation);
                intent.putExtra("timestamp", mTimestamp);
                intent.putExtra("description", mEtDescription.getText().toString().trim());
                intent.putExtra("title", mEtTitle.getText().toString().trim());
                intent.putExtra("url", mUrl);
                intent.putExtra("id", mChannel);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.tvSkip:
                finish();
                break;
        }
    }

    private boolean validateTitle() {
        if (mEtTitle.getText().toString().trim().length() == 0) {
            Toast.makeText(this, "Please enter title", Toast.LENGTH_SHORT).show();
            return false;
        } else return true;
    }

    private void sendDataToImageStorage(final String picturePath, final View view) {
        final Image imageModel = new Image();
        imageModel.setUserId(SharedPreferenceManager.getInstance(ApplicationLoader.getInstance()).getString(Constants.KEY_USER_ID));
        imageModel.setLocation(mLocation);
        imageModel.setTimestamp(mTimestamp);
        imageModel.setDescription(mEtDescription.getText().toString().trim());
        imageModel.setTitle(mEtTitle.getText().toString().trim());

        storageRef = storage.getReferenceFromUrl("gs://photoshareapp-2368d.appspot.com");


        mProgressBar.setVisibility(View.VISIBLE);
        Uri file = Uri.fromFile(new File(picturePath));
        StorageReference riversRef = storageRef.child("images/" + file.getLastPathSegment());
        UploadTask uploadTask = riversRef.putFile(file);

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Toast.makeText(TakePhotoActivity.this, "Upload Failed", Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                imageModel.setUrl(String.valueOf(downloadUrl));
                mUrl = String.valueOf(downloadUrl);

                SaveImageDetails.writeNewImage(mChannel,
                        String.valueOf(downloadUrl),
                        SharedPreferenceManager.getInstance(ApplicationLoader.getInstance()).getString(Constants.KEY_USER_ID),
                        mLocation, mTimestamp, mEtDescription.getText().toString().trim(), mEtTitle.getText().toString().trim());

                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(TakePhotoActivity.this, "Upload Successful", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
