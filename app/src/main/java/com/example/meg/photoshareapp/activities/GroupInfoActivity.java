package com.example.meg.photoshareapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.adapters.LocationAdapter;
import com.example.meg.photoshareapp.adapters.MembersAdapter;
import com.example.meg.photoshareapp.models.Group;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupInfoActivity extends AppCompatActivity {

    private RecyclerView mMembersRecyclerView, mLocationRecyclerView;
    private MembersAdapter mMemberAdapter;
    private LocationAdapter mLocationAdapter;
    private Group mGroup;
    private LinearLayout mlocationLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_info);
        getBundleData();
        initToolbar();
        initViews();
    }

    private void getBundleData() {
        if (getIntent().getExtras() != null) {
            mGroup = (Group) getIntent().getSerializableExtra("group");
        }
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setTitle(getResources().getString(R.string.group_info));
    }

    private void initViews() {
        mMembersRecyclerView = findViewById(R.id.membersRecyclerView);
        mMemberAdapter = new MembersAdapter(this);
        mMembersRecyclerView.setHasFixedSize(true);
        LinearLayoutManager recentTransactionLinearLayoutManager = new LinearLayoutManager(ApplicationLoader.getInstance());
        recentTransactionLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mMembersRecyclerView.setLayoutManager(recentTransactionLinearLayoutManager);
        mMembersRecyclerView.setAdapter(mMemberAdapter);

        mLocationRecyclerView = findViewById(R.id.locationRecyclerView);
        mLocationAdapter = new LocationAdapter(this, mGroup);
        mLocationRecyclerView.setHasFixedSize(true);

        if (recentTransactionLinearLayoutManager != null) {
            recentTransactionLinearLayoutManager = null;
        }

        LinearLayoutManager locationLinearLayoutManager = new LinearLayoutManager(ApplicationLoader.getInstance());
        locationLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLocationRecyclerView.setLayoutManager(locationLinearLayoutManager);
        mLocationRecyclerView.setAdapter(mLocationAdapter);

        mlocationLayout = findViewById(R.id.locationLayout);

        mMemberAdapter.setDataInAdapter(mGroup.getMembers());
        if (getLocationSet() == null) {
            mlocationLayout.setVisibility(View.GONE);
        } else {
            mlocationLayout.setVisibility(View.VISIBLE);
            mLocationAdapter.setDataInAdapter(getLocationSet());
        }

    }

    private List<String> getLocationSet() {
        String location;
        Map<String, Integer> temp = new HashMap<>();

        if (mGroup.getImages() == null) {
            return null;
        } else {

            for (int i = 0; i < mGroup.getImages().size(); i++) {
                location = mGroup.getImages().get(i).getLocation();

                if (temp.isEmpty() || !(temp.containsKey(location))) {
                    temp.put(location, 1);
                }
            }
            List<String> keys = new ArrayList<>(temp.keySet());
            return keys;
        }

    }
}
