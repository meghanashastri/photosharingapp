package com.example.meg.photoshareapp.utils;

public class Constants {

    public static final String KEY_EMAIL = "email";
    public static final String KEY_NAME = "name";
    public static final String KEY_USER_ID = "user_id";
    public static final String FIREBASE_URL = "https://photoshareapp-2368d.firebaseio.com/";

    public static final int TAKE_PHOTO_ACTIVITY_FLAG = 0;
    public static final int ADD_MEMBER__ACTIVITY_FLAG = 1;
    public static final int SPLASH_SCREEN_FLAG = 2;
    public static final String FLAG = "flag";

}
