package com.example.meg.photoshareapp.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.activities.CreateGroupActivity;
import com.example.meg.photoshareapp.activities.HomeActivity;
import com.example.meg.photoshareapp.adapters.GroupsAdapter;
import com.example.meg.photoshareapp.models.Group;
import com.example.meg.photoshareapp.models.Image;
import com.example.meg.photoshareapp.models.User;
import com.example.meg.photoshareapp.utils.Constants;
import com.example.meg.photoshareapp.utils.FontManager;
import com.example.meg.photoshareapp.utils.SaveGroupDetails;
import com.example.meg.photoshareapp.utils.SharedPreferenceManager;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GroupsFragment extends Fragment implements GroupsAdapter.onGroupSelect, View.OnClickListener {

    private List<Group> groupList = new ArrayList<>();
    private RecyclerView mGroupRecyclerView;
    private GroupsAdapter mAdapter;
    private TextView mtvNoGroupText, mAddGroup;
    private RelativeLayout mProgressBar;
    private int mFlag;
    private List<Group> mSelectedList = new ArrayList<>();
    private List<Image> mImageList = new ArrayList<>();
    private Image mImageModel = new Image();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_groups,
                container, false);
        Firebase.setAndroidContext(ApplicationLoader.getInstance());
        getBundleData();
        initViews(rootView);

        return rootView;
    }


    private void getBundleData() {
        if (getActivity().getIntent().getExtras() != null) {
            mFlag = getActivity().getIntent().getIntExtra(Constants.FLAG, 0);
            if (getActivity().getIntent().getIntExtra(Constants.FLAG, 0) == Constants.TAKE_PHOTO_ACTIVITY_FLAG) {
                String mLocation = getActivity().getIntent().getStringExtra("location");
                String mTitle = getActivity().getIntent().getStringExtra("title");
                String mID = getActivity().getIntent().getStringExtra("id");
                String mUrl = getActivity().getIntent().getStringExtra("url");
                String mTimestamp = getActivity().getIntent().getStringExtra("timestamp");
                String mDescription = getActivity().getIntent().getStringExtra("description");
                String mUserID = getActivity().getIntent().getStringExtra("userId");
                mImageModel.setUserId(mUserID);
                mImageModel.setLocation(mLocation);
                mImageModel.setDescription(mDescription);
                mImageModel.setTitle(mTitle);
                mImageModel.setUrl(mUrl);
                mImageModel.setTimestamp(mTimestamp);
                mImageModel.setUserId(SharedPreferenceManager.getInstance(ApplicationLoader.getInstance()).getString(Constants.KEY_USER_ID));
                mImageModel.setImageId(mID);

                mImageList.add(mImageModel);
            }
        }
    }

    private void initViews(ViewGroup rootView) {
        mAddGroup = rootView.findViewById(R.id.addGroup);
        Typeface iconFont = FontManager.getTypeface(ApplicationLoader.getInstance(), FontManager.MATERIALICON);
        mAddGroup.setTypeface(iconFont);
        mAddGroup.setOnClickListener(this);

        mGroupRecyclerView = rootView.findViewById(R.id.groupRecyclerView);
        mAdapter = new GroupsAdapter(ApplicationLoader.getInstance(), mFlag);
        mAdapter.setGroupSelectedListener(this);
        mGroupRecyclerView.setHasFixedSize(true);
        LinearLayoutManager recentTransactionLinearLayoutManager = new LinearLayoutManager(ApplicationLoader.getInstance());
        recentTransactionLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mGroupRecyclerView.setLayoutManager(recentTransactionLinearLayoutManager);
        mGroupRecyclerView.setAdapter(mAdapter);
        mtvNoGroupText = rootView.findViewById(R.id.tvNoGroupText);
        mProgressBar = rootView.findViewById(R.id.progressBarLayout);
        if (mFlag == 0) {
            mAddGroup.setVisibility(View.GONE);
            mAddGroup.setText(ApplicationLoader.getInstance().getResources().getString(R.string.share_hex_code));
        } else {
            mAddGroup.setText(ApplicationLoader.getInstance().getResources().getString(R.string.add_image_hex_code));
        }
    }


    private void getGroupDetailsFromFirebaseDB() {
        mProgressBar.setVisibility(View.VISIBLE);
        Firebase images = new Firebase(Constants.FIREBASE_URL).child("groups");
        images.orderByChild("created_by_user_id").equalTo(SharedPreferenceManager.getInstance(ApplicationLoader.getInstance()).
                getString(Constants.KEY_USER_ID)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    //empty groups list
                    mtvNoGroupText.setVisibility(View.VISIBLE);
                    mGroupRecyclerView.setVisibility(View.GONE);
                } else {

                    Group groupModel;
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        groupModel = new Group();
                        try {
                            Object object = child.getValue();
                            Gson gson = new Gson();
                            String json = gson.toJson(object);

                            JSONObject jsonObject = new JSONObject(json);

                            groupModel.setGroupId(child.getKey());
                            groupModel.setDescription(jsonObject.getString("description"));
                            groupModel.setCreated_by_user_id(jsonObject.getString("created_by_user_id"));
                            String member = jsonObject.getString("members");

                            if (jsonObject.has("images")) {
                                String image = jsonObject.getString("images");
                                Gson gsonImage = new Gson();
                                Type typeImage = new TypeToken<List<Image>>() {
                                }.getType();
                                List<Image> imageList = gsonImage.fromJson(image, typeImage);
                                groupModel.setImages(imageList);
                            }

                            Gson gsonMember = new Gson();
                            Type typeMember = new TypeToken<List<User>>() {
                            }.getType();
                            List<User> memberList = gsonMember.fromJson(member, typeMember);
                            groupModel.setMembers(memberList);

                            groupModel.setTitle(jsonObject.getString("title"));


                            groupList.add(groupModel);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if (groupList != null) {
                        mtvNoGroupText.setVisibility(View.GONE);
                        mGroupRecyclerView.setVisibility(View.VISIBLE);
                        mAdapter.setDataInAdapter(groupList);
                    }
                }
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(ApplicationLoader.getInstance(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onGroupSelected(Group group) {
        mAddGroup.setVisibility(View.VISIBLE);
        mSelectedList.add(group);

        for (int i = 0; i < mSelectedList.size(); i++) {
            Log.d("list", mSelectedList.get(i).getTitle());
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.addGroup:
                if (mFlag == 0) {
                    //share flow
                    for (int i = 0; i < mSelectedList.size(); i++) {
                        if (mSelectedList.get(i).getImages() != null) {
                            mImageList = mSelectedList.get(i).getImages();
                            mImageList.add(mImageModel);
                            SaveGroupDetails.writeGroupDetails(mSelectedList.get(i).getGroupId(), mSelectedList.get(i).getMembers(),
                                    mSelectedList.get(i).getTitle(), mSelectedList.get(i).getDescription(),
                                    mImageList);
                            Intent intent = new Intent(ApplicationLoader.getInstance(), HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            getActivity().finish();
                        } else {
                            SaveGroupDetails.writeGroupDetails(mSelectedList.get(i).getGroupId(), mSelectedList.get(i).getMembers(),
                                    mSelectedList.get(i).getTitle(), mSelectedList.get(i).getDescription(),
                                    mImageList);
                            Intent intent = new Intent(ApplicationLoader.getInstance(), HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                } else {
                    //create group flow
                    Intent intent = new Intent(ApplicationLoader.getInstance(), CreateGroupActivity.class);
                    startActivity(intent);
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getGroupDetailsFromFirebaseDB();
    }

    @Override
    public void onStop() {
        super.onStop();
        mAdapter.clear();
    }
}
