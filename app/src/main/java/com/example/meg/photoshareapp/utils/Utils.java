package com.example.meg.photoshareapp.utils;

import com.example.meg.photoshareapp.models.Image;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class Utils {

    /**
     * Showing timestamp according to the default timezone
     *
     * @return
     */
    public static String getTimestamp() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'", Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String timestamp = simpleDateFormat.format(new Date());


        int index = timestamp.indexOf(".");
        String timestampToFormat = timestamp.substring(0, index);
        try {
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); // According to your Server TimeStamp
            formatter.setTimeZone(TimeZone.getTimeZone("UTC")); //your Server Time Zone
            Date value = formatter.parse(timestampToFormat); // Parse your date

            SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM dd',' yyyy"); //this format changeable according to your choice
            dateFormatter.setTimeZone(tz);
            timestampToFormat = dateFormatter.format(value);

        } catch (Exception e) {
            timestampToFormat = "00-00-0000 00:00";

        }
        return timestampToFormat;
    }


    public static List<String> getImageDate(List<Image> images) {
        String timestamp;
        Map<String, Integer> temp = new HashMap<>();

        if (images == null) {
            return null;
        } else {

            for (int i = 0; i < images.size(); i++) {
                timestamp = images.get(i).getTimestamp();

                if (temp.isEmpty() || !(temp.containsKey(timestamp))) {
                    temp.put(timestamp, 1);
                }
            }
            List<String> keys = new ArrayList<>(temp.keySet());

            return keys;
        }
    }

    public static List<Image> getUniqueDate(List<Image> images, String date) {
        List<Image> imageUrl = new ArrayList<>();

        for (int i = 0; i < images.size(); i++) {
            if (images.get(i).getTimestamp().equals(date)) {
                imageUrl.add(images.get(i));
            }
        }
        return imageUrl;
    }

    public static HashMap<String, List<Image>> getList(List<Image> imageList) {
        List<String> date = Utils.getImageDate(imageList);

        HashMap<String, List<Image>> map = new HashMap<>();
        List<Image> image = new ArrayList<>();

        if (date.size() == 0) {
            return null;
        } else {

            for (int i = 0; i < date.size(); i++) {
                image = Utils.getUniqueDate(imageList, date.get(i));
                map.put(date.get(i), image);
            }

            return map;
        }
    }

    public static Date[] getDatesArray(List<String> dates) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM dd',' yyyy");
        Date[] arrayOfDates = new Date[dates.size()];
        for (int index = 0; index < dates.size(); index++) {
            try {
                arrayOfDates[index] = dateFormatter.parse(dates.get(index));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return arrayOfDates;
    }


    public static List<String> sortDateArray(Date[] arrayOfDates, List<String> dates) {
        String[] arr = dates.toArray(new String[dates.size()]);

        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM dd',' yyyy");
        Arrays.sort(arrayOfDates);
        for (int index = 0; index < dates.size(); index++) {
            arr[index] = dateFormatter.format(arrayOfDates[index]);
        }

        if (arr.length > 0) {
            dates = Arrays.asList(arr);
            Collections.reverse(dates);
        }

        return dates;
    }


    public static LinkedHashMap<String, List<Image>> getSortedDateAndImageList(List<Image> imageList) {

        if (Utils.getList(imageList) != null) {
            HashMap<String, List<Image>> map = Utils.getList(imageList);
            LinkedHashMap<String, List<Image>> sortedMap = new LinkedHashMap<>();
            List<Image> image = new ArrayList<>();


            Date[] arrayOfDates = Utils.getDatesArray(new ArrayList<String>(map.keySet()));

            List<String> sortedDateList = Utils.sortDateArray(arrayOfDates, new ArrayList<String>(map.keySet()));


            for (int i = 0; i < sortedDateList.size(); i++) {
                image = Utils.getUniqueDate(imageList, sortedDateList.get(i));
                sortedMap.put(sortedDateList.get(i), image);
            }


            return sortedMap;
        } else {
            return null;
        }

    }
}
