package com.example.meg.photoshareapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.activities.HomeActivity;
import com.example.meg.photoshareapp.models.Image;
import com.example.meg.photoshareapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {

    private Context mContext;
    private List<Image> mImagesList;

    public ImagesAdapter(Context context) {
        mContext = context;
        mImagesList = new ArrayList<>();
    }

    public void setDataInAdapter(List<Image> member) {
        mImagesList = member;
        notifyDataSetChanged();
    }

    public void clear() {
        final int size = mImagesList.size();
        mImagesList.clear();
        notifyItemRangeRemoved(0, size);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_image, parent, false);
        return new ImagesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Image image = mImagesList.get(position);
        if (image != null) {
            holder.tvImageTitle.setText(image.getTitle());
            holder.imageView.setImageUrl(image.getUrl(), ApplicationLoader.getInstance().getImageLoader());

            holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    Intent intent = new Intent(ApplicationLoader.getInstance(), HomeActivity.class);
                    intent.putExtra(Constants.FLAG, Constants.TAKE_PHOTO_ACTIVITY_FLAG);
                    intent.putExtra("position",1);
                    intent.putExtra("location", image.getLocation());
                    intent.putExtra("timestamp", image.getTimestamp());
                    intent.putExtra("description", image.getDescription());
                    intent.putExtra("title", image.getTitle());
                    intent.putExtra("url", image.getUrl());
                    intent.putExtra("id", image.getImageId());
                    intent.putExtra("userId",image.getUserId());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    ApplicationLoader.getInstance().startActivity(intent);
                    return false;
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mImagesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private NetworkImageView imageView;
        private TextView tvImageTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            tvImageTitle = itemView.findViewById(R.id.tvImageTitle);
            mView = itemView;
        }

    }
}
