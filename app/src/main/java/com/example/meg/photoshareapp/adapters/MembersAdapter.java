package com.example.meg.photoshareapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.models.User;

import java.util.ArrayList;
import java.util.List;

public class MembersAdapter extends RecyclerView.Adapter<MembersAdapter.ViewHolder> {


    private Context mContext;
    private List<User> mMembersList;

    public MembersAdapter(Context context) {
        mContext = context;
        mMembersList = new ArrayList<>();
    }

    public void setDataInAdapter(List<User> member) {
        mMembersList = member;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_add_member, parent, false);
        return new MembersAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvCancel.setVisibility(View.GONE);
        User group = mMembersList.get(position);
        if (group != null) {
            holder.tvMemberSelected.setText(mMembersList.get(position).getUsername());
        }
    }

    @Override
    public int getItemCount() {
        return mMembersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView tvCancel, tvMemberSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCancel = itemView.findViewById(R.id.tvCancel);
            tvMemberSelected = itemView.findViewById(R.id.tvMemberSelected);
            mView = itemView;
        }

    }

}
