package com.example.meg.photoshareapp.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.models.User;
import com.example.meg.photoshareapp.utils.FontManager;

import java.util.ArrayList;
import java.util.List;

public class AddMemberAdapter extends RecyclerView.Adapter<AddMemberAdapter.ViewHolder> {

    private Context mContext;
    private List<User> mMemberList;

    public AddMemberAdapter(Context context) {
        mContext = context;
        mMemberList = new ArrayList<>();
    }

    public void setDataInAdapter(List<User> member) {
        mMemberList = member;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_add_member, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        User user = mMemberList.get(position);
        Typeface iconFont = FontManager.getTypeface(ApplicationLoader.getInstance(), FontManager.MATERIALICON);
        holder.tvCancel.setTypeface(iconFont);
        if (user != null) {
            holder.tvMemberSelected.setText(user.getUsername());

            holder.tvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mMemberList.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mMemberList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView tvCancel, tvMemberSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCancel = itemView.findViewById(R.id.tvCancel);
            tvMemberSelected = itemView.findViewById(R.id.tvMemberSelected);
            mView = itemView;
        }

    }
}
