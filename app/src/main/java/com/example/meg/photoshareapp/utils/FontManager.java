package com.example.meg.photoshareapp.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by meg on 29/11/17.
 */

public class FontManager {

    public static final String ROOT = "fonts/";

    public static final String MATERIALICON = ROOT + "MaterialIcons.ttf";

    public static Typeface getTypeface(Context context, String font) {
        return Typeface.createFromAsset(context.getAssets(), font);
    }
}

