package com.example.meg.photoshareapp.utils;

import com.example.meg.photoshareapp.models.Image;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SaveImageDetails {

    public static void writeNewImage(String id, String url, String userId, String location, String timestamp,
                                     String description, String title) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        Image image = new Image(url, userId, location, timestamp, description, title);
        mDatabase.child("images").child(id).setValue(image);
    }

}
