package com.example.meg.photoshareapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.fragments.GroupsFragment;
import com.example.meg.photoshareapp.fragments.ImagesFragment;
import com.example.meg.photoshareapp.utils.Constants;
import com.example.meg.photoshareapp.utils.SharedPreferenceManager;
import com.google.firebase.auth.FirebaseAuth;

public class HomeActivity extends AppCompatActivity {

    private ImagesFragment mImageFragment;
    private GroupsFragment mGroupFragment;
    private TabLayout mTabLayout;
    private int mFragmentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initToolbar();
        initViews();

        if (getIntent().getExtras()!=null){
            mFragmentPosition = getIntent().getIntExtra("position",0);
        }else {
            mFragmentPosition=0;
        }

        bindWidgetsWithAnEvent();
        setUpTabLayout();

        setCurrentTabFragment(mFragmentPosition);
        TabLayout.Tab tab = mTabLayout.getTabAt(mFragmentPosition);
        tab.select();


    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (SharedPreferenceManager.getInstance(ApplicationLoader.getInstance()).getString(Constants.KEY_NAME) != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setTitle(SharedPreferenceManager.getInstance(ApplicationLoader.getInstance()).
                    getString(Constants.KEY_NAME));
        }
    }

    private void initViews() {

        mTabLayout = findViewById(R.id.tabs);
    }

    private void setUpTabLayout() {
        mImageFragment = new ImagesFragment();
        mGroupFragment = new GroupsFragment();
        mTabLayout.addTab(mTabLayout.newTab().setText(getResources().getString(R.string.image)), true);

        mTabLayout.addTab(mTabLayout.newTab().setText(getResources().getString(R.string.group)));
    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 1:
                replaceFragment(mGroupFragment, "GroupFragment");
                break;
            case 0:
                replaceFragment(mImageFragment, "ImageFragment");
                break;
        }
    }

    public void replaceFragment(Fragment fragment, String TAG) {
        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.fragment_holder, fragment, TAG);
        mFragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                SharedPreferenceManager.getInstance(this).putIsLoggedIn(false);
                FirebaseAuth.getInstance().signOut();
                startActivity((new Intent()).setClass(getApplicationContext(), SignInActivity.class));
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void bindWidgetsWithAnEvent() {
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }
}
