package com.example.meg.photoshareapp.utils;

import com.example.meg.photoshareapp.models.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SaveUserDetails {

    public static void writeNewUser(String userId, String name, String email) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        User user = new User(name, email);
        mDatabase.child("users").child(userId).setValue(user);
    }
}
