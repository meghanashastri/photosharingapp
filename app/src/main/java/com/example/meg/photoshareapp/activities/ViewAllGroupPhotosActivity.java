package com.example.meg.photoshareapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.adapters.ImagesAdapter;
import com.example.meg.photoshareapp.adapters.SectionRecyclerAdapter;
import com.example.meg.photoshareapp.models.Group;
import com.example.meg.photoshareapp.models.Image;
import com.example.meg.photoshareapp.models.SectionModel;
import com.example.meg.photoshareapp.utils.RecyclerViewType;
import com.example.meg.photoshareapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.example.meg.photoshareapp.utils.RecyclerViewType.GRID;
import static com.example.meg.photoshareapp.utils.RecyclerViewType.LINEAR_VERTICAL;

public class ViewAllGroupPhotosActivity extends AppCompatActivity {

    private RecyclerView mimageRecyclerView;
    private RecyclerViewType recyclerViewType = GRID;
    private SectionRecyclerAdapter mAdater;
    private List<Image> imageList = new ArrayList<>();
    private TextView mtvNoImageText;
    private RelativeLayout mProgressBar;
    private ImagesAdapter imagesAdapter;
    private Group mGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_group_photos);
        getBundleData();
        initToolbar();
        initViews();
        setData();
    }

    private void getBundleData() {
        if (getIntent().getExtras() != null) {
            mGroup = (Group) getIntent().getSerializableExtra("group");
        }
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setTitle(getResources().getString(R.string.view_photos));
    }

    private void initViews() {
        imagesAdapter = new ImagesAdapter(ApplicationLoader.getInstance());
        TextView addImage = findViewById(R.id.addImage);
        addImage.setVisibility(View.GONE);
        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ApplicationLoader.getInstance(), TakePhotoActivity.class);
                startActivity(intent);
            }
        });


        mimageRecyclerView = findViewById(R.id.imageRecyclerView);
        mimageRecyclerView.setHasFixedSize(true);
        LinearLayoutManager recentTransactionLinearLayoutManager = new LinearLayoutManager(ApplicationLoader.getInstance());
        mimageRecyclerView.setLayoutManager(recentTransactionLinearLayoutManager);

        mtvNoImageText = findViewById(R.id.tvNoImageText);
        mProgressBar = findViewById(R.id.progressBarLayout);
    }

    private void setData() {
        if (mGroup.getImages() != null) {
            mtvNoImageText.setVisibility(View.GONE);
            mimageRecyclerView.setVisibility(View.VISIBLE);


            if (Utils.getSortedDateAndImageList(mGroup.getImages()) != null) {

                LinkedHashMap<String, List<Image>> map = Utils.getSortedDateAndImageList(mGroup.getImages());

                if (map != null) {
                    ArrayList<SectionModel> sectionModelArrayList = new ArrayList<>();
                    List<String> keys = new ArrayList<>();
                    List<Image> value = new ArrayList<>();
                    for (Map.Entry<String, List<Image>> ee : map.entrySet()) {
                        String key = ee.getKey();
                        List<Image> values = ee.getValue();
                        sectionModelArrayList.add(new SectionModel(ee.getKey(), ee.getValue()));
                        keys.add(key);
                        for (int i = 0; i < ee.getValue().size(); i++) {
                            value.add(values.get(i));
                        }
                    }

                    mAdater = new SectionRecyclerAdapter(ApplicationLoader.getInstance(), recyclerViewType, sectionModelArrayList);
                    mimageRecyclerView.setAdapter(mAdater);
                }
            }
        }
    }
}
