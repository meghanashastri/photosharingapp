package com.example.meg.photoshareapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.activities.GroupInfoActivity;
import com.example.meg.photoshareapp.activities.ViewAllGroupPhotosActivity;
import com.example.meg.photoshareapp.models.Group;
import com.example.meg.photoshareapp.utils.FontManager;

import java.util.ArrayList;
import java.util.List;

public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.ViewHolder> {

    private Context mContext;
    private List<Group> mGroupList;
    private onGroupSelect mListener;
    private int mFlag;

    public GroupsAdapter(Context context, int mFlag) {
        mContext = context;
        mGroupList = new ArrayList<>();
        this.mFlag = mFlag;
    }

    public void setDataInAdapter(List<Group> member) {
        mGroupList = member;
        notifyDataSetChanged();
    }

    public void clear() {
        final int size = mGroupList.size();
        mGroupList.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void setGroupSelectedListener(onGroupSelect listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_group, parent, false);
        return new GroupsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final Group group = mGroupList.get(position);
        Typeface iconFont = FontManager.getTypeface(ApplicationLoader.getInstance(), FontManager.MATERIALICON);
        holder.tvGroupInfo.setTypeface(iconFont);
        if (group != null) {
            holder.tvGroupName.setText(group.getTitle());

            if (group.getImages() == null) {
                holder.tvNumberOfPhotosAndMembers.setText("0 Photos . " + group.getMembers().size() + " Members");

            } else {
                holder.tvNumberOfPhotosAndMembers.setText(group.getImages().size() + " Photos . " + group.getMembers().size() + " Members");
            }

            if (mFlag == 0) {
                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mListener.onGroupSelected(group);
                        holder.mView.setBackgroundColor(ApplicationLoader.getInstance().getResources().getColor(R.color.selected_color));
                    }
                });
            } else {
                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, ViewAllGroupPhotosActivity.class);
                        intent.putExtra("group", group);
                        mContext.startActivity(intent);
                    }
                });
            }

            holder.tvGroupInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, GroupInfoActivity.class);
                    intent.putExtra("group", group);
                    mContext.startActivity(intent);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mGroupList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView tvGroupInfo, tvGroupName, tvNumberOfPhotosAndMembers;

        public ViewHolder(View itemView) {
            super(itemView);
            tvNumberOfPhotosAndMembers = itemView.findViewById(R.id.tvNumberOfPhotosAndMembers);
            tvGroupName = itemView.findViewById(R.id.tvGroupName);
            tvGroupInfo = itemView.findViewById(R.id.tvGroupInfo);
            mView = itemView;
        }

    }

    public interface onGroupSelect {
        void onGroupSelected(Group group);
    }
}
