package com.example.meg.photoshareapp.utils;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.models.Group;
import com.example.meg.photoshareapp.models.Image;
import com.example.meg.photoshareapp.models.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class SaveGroupDetails {

    public static void writeGroupDetails(String id, List<User> userList, String title, String description, List<Image> images) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        Group group = new Group();
        group.setCreated_by_user_id(SharedPreferenceManager.getInstance(ApplicationLoader.getInstance()).getString(Constants.KEY_USER_ID));
        group.setTitle(title);
        group.setDescription(description);
        group.setMembers(userList);
        group.setImages(images);

        mDatabase.child("groups").child(id).setValue(group);

    }
}
