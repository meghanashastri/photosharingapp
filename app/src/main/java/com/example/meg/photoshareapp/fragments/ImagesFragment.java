package com.example.meg.photoshareapp.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.activities.TakePhotoActivity;
import com.example.meg.photoshareapp.adapters.ImagesAdapter;
import com.example.meg.photoshareapp.adapters.SectionRecyclerAdapter;
import com.example.meg.photoshareapp.models.Image;
import com.example.meg.photoshareapp.models.SectionModel;
import com.example.meg.photoshareapp.utils.Constants;
import com.example.meg.photoshareapp.utils.FontManager;
import com.example.meg.photoshareapp.utils.RecyclerViewType;
import com.example.meg.photoshareapp.utils.SharedPreferenceManager;
import com.example.meg.photoshareapp.utils.Utils;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.example.meg.photoshareapp.utils.RecyclerViewType.GRID;

public class ImagesFragment extends Fragment {

    private RecyclerView mimageRecyclerView;
    private RecyclerViewType recyclerViewType = GRID;
    private SectionRecyclerAdapter mAdater;
    private List<Image> imageList = new ArrayList<>();
    private TextView mtvNoImageText;
    private RelativeLayout mProgressBar;
    private ImagesAdapter imagesAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_images,
                container, false);
        Firebase.setAndroidContext(ApplicationLoader.getInstance());

        initViews(rootView);

        return rootView;
    }

    private void initViews(ViewGroup rootView) {
        imagesAdapter = new ImagesAdapter(ApplicationLoader.getInstance());
        TextView addImage = rootView.findViewById(R.id.addImage);
        Typeface iconFont = FontManager.getTypeface(ApplicationLoader.getInstance(), FontManager.MATERIALICON);
        addImage.setTypeface(iconFont);
        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ApplicationLoader.getInstance(), TakePhotoActivity.class);
                startActivity(intent);
            }
        });


        mimageRecyclerView = rootView.findViewById(R.id.imageRecyclerView);
        mimageRecyclerView.setHasFixedSize(true);
        LinearLayoutManager recentTransactionLinearLayoutManager = new LinearLayoutManager(ApplicationLoader.getInstance());
        mimageRecyclerView.setLayoutManager(recentTransactionLinearLayoutManager);

        mtvNoImageText = rootView.findViewById(R.id.tvNoImageText);
        mProgressBar = rootView.findViewById(R.id.progressBarLayout);
    }


    private void getImageDetailsFromFirebaseDB() {
        mProgressBar.setVisibility(View.VISIBLE);

        Firebase images = new Firebase(Constants.FIREBASE_URL).child("images");
        images.orderByChild("userId").equalTo(SharedPreferenceManager.getInstance(ApplicationLoader.getInstance()).
                getString(Constants.KEY_USER_ID)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    //empty images list
                    mimageRecyclerView.setVisibility(View.GONE);
                    mtvNoImageText.setVisibility(View.VISIBLE);

                } else {

                    Image imageModel;
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        imageModel = new Image();
                        try {
                            Object object = child.getValue();
                            Gson gson = new Gson();
                            String json = gson.toJson(object);

                            JSONObject jsonObject = new JSONObject(json);

                            imageModel.setImageId(child.getKey());
                            imageModel.setUrl(jsonObject.getString("url"));
                            imageModel.setTimestamp(jsonObject.getString("timestamp"));
                            imageModel.setTitle(jsonObject.getString("title"));
                            imageModel.setDescription(jsonObject.getString("description"));
                            imageModel.setLocation(jsonObject.getString("location"));
                            imageModel.setUserId(jsonObject.getString("userId"));
                            imageList.add(imageModel);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if (imageList != null) {
                        mtvNoImageText.setVisibility(View.GONE);
                        mimageRecyclerView.setVisibility(View.VISIBLE);

                        if (Utils.getSortedDateAndImageList(imageList) != null) {
                            LinkedHashMap<String, List<Image>> map = Utils.getSortedDateAndImageList(imageList);

                            if (map != null) {
                                ArrayList<SectionModel> sectionModelArrayList = new ArrayList<>();
                                List<String> keys = new ArrayList<>();
                                List<Image> value = new ArrayList<>();
                                for (Map.Entry<String, List<Image>> ee : map.entrySet()) {
                                    String key = ee.getKey();
                                    List<Image> values = ee.getValue();
                                    sectionModelArrayList.add(new SectionModel(ee.getKey(), ee.getValue()));
                                    keys.add(key);
                                    for (int i = 0; i < ee.getValue().size(); i++) {
                                        value.add(values.get(i));
                                    }
                                }

                                mAdater = new SectionRecyclerAdapter(ApplicationLoader.getInstance(), recyclerViewType, sectionModelArrayList);
                                mimageRecyclerView.setAdapter(mAdater);
                            }
                        }
                    }
                }
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(ApplicationLoader.getInstance(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getImageDetailsFromFirebaseDB();
    }

}
