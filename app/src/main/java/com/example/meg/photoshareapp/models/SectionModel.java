package com.example.meg.photoshareapp.models;

import java.util.ArrayList;
import java.util.List;

public class SectionModel {
    private String sectionLabel;
    private List<Image> itemArrayList;

    public SectionModel(String sectionLabel, List<Image> itemArrayList) {
        this.sectionLabel = sectionLabel;
        this.itemArrayList = itemArrayList;
    }

    public String getSectionLabel() {
        return sectionLabel;
    }

    public List<Image> getItemArrayList() {
        return itemArrayList;
    }
}
