package com.example.meg.photoshareapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.meg.photoshareapp.activities.HomeActivity;
import com.example.meg.photoshareapp.activities.SignInActivity;
import com.example.meg.photoshareapp.utils.Constants;
import com.example.meg.photoshareapp.utils.SharedPreferenceManager;

public class SplashScreen extends AppCompatActivity {

    private static final int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (SharedPreferenceManager.getInstance(getApplicationContext()).getIsLoggedIn()) {
                    Intent intent = new Intent();
                    intent.setClass(SplashScreen.this, HomeActivity.class);
                    intent.putExtra(Constants.FLAG, Constants.SPLASH_SCREEN_FLAG);
                    startActivity(intent);
                    SplashScreen.this.finish();
                } else {
                    Intent intent = new Intent();
                    intent.setClass(SplashScreen.this, SignInActivity.class);
                    startActivity(intent);
                    SplashScreen.this.finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }
}
