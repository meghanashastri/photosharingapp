package com.example.meg.photoshareapp.models;

import java.io.Serializable;

public class Image implements Serializable {
    private String imageId;
    private String url;
    private String userId;
    private String location;
    private String timestamp;
    private String description;
    private String title;

    public Image() {

    }

    public Image(String url, String userId, String location, String timestamp, String description, String title) {
        this.url = url;
        this.userId = userId;
        this.location = location;
        this.timestamp = timestamp;
        this.description = description;
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

}
