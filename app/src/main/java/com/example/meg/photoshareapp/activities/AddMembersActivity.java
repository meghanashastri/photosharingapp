package com.example.meg.photoshareapp.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meg.photoshareapp.ApplicationLoader;
import com.example.meg.photoshareapp.R;
import com.example.meg.photoshareapp.adapters.AddMemberAdapter;
import com.example.meg.photoshareapp.models.User;
import com.example.meg.photoshareapp.utils.Constants;
import com.example.meg.photoshareapp.utils.FontManager;
import com.example.meg.photoshareapp.utils.SaveGroupDetails;
import com.example.meg.photoshareapp.utils.SharedPreferenceManager;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AddMembersActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText mEtSearchByEmail;
    private static final String FIREBASE_URL = "https://photoshareapp-2368d.firebaseio.com/";
    private RelativeLayout mMemberDetailsLayout, minviteLayout, mSaveLayout;
    private TextView mtvSearchName, mtvNoMemberText, mtvYes, mtvNo;
    private String mMemberName, mMemberEmail, mMemberUserId, mGroupName, mGroupDescription;
    private RecyclerView mMemberRecyclerView;
    private AddMemberAdapter mAdater;
    private List<User> mMemberList = new ArrayList<>();
    private User mUserModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_add_members);
        getBundleData();
        initToolbar();
        initViews();
    }

    private void getBundleData() {
        //retrieving notification response
        if (getIntent().getExtras() != null) {
            mGroupName = getIntent().getStringExtra("groupName");
            mGroupDescription = getIntent().getStringExtra("groupDescription");
        }
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setTitle(getResources().getString(R.string.add_members));
    }

    private void initViews() {
        Typeface iconFont = FontManager.getTypeface(ApplicationLoader.getInstance(), FontManager.MATERIALICON);
        mEtSearchByEmail = findViewById(R.id.etSearchByEmail);
        TextView tvSearch = findViewById(R.id.tvSearch);
        tvSearch.setTypeface(iconFont);
        tvSearch.setOnClickListener(this);
        mMemberDetailsLayout = findViewById(R.id.memberDetailsLayout);
        mtvSearchName = findViewById(R.id.tvSearchName);
        minviteLayout = findViewById(R.id.inviteLayout);
        TextView tvInvite = findViewById(R.id.tvInvite);
        tvInvite.setOnClickListener(this);
        TextView tvSelectMember = findViewById(R.id.tvSelectMember);
        tvSelectMember.setOnClickListener(this);
        mtvNoMemberText = findViewById(R.id.tvNoMemberText);
        mMemberRecyclerView = findViewById(R.id.memberRecyclerView);
        mAdater = new AddMemberAdapter(this);
        mMemberRecyclerView.setHasFixedSize(true);
        LinearLayoutManager recentTransactionLinearLayoutManager = new LinearLayoutManager(this);
        recentTransactionLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mMemberRecyclerView.setLayoutManager(recentTransactionLinearLayoutManager);
        mMemberRecyclerView.setAdapter(mAdater);
        mSaveLayout = findViewById(R.id.saveLayout);
        mtvYes = findViewById(R.id.tvYes);
        mtvYes.setTypeface(iconFont);
        mtvYes.setOnClickListener(this);
        mtvNo = findViewById(R.id.tvNo);
        mtvNo.setTypeface(iconFont);
        mtvNo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvSearch:
                if (mEtSearchByEmail.getText().toString().trim().length() > 0) {
                    if (isValidEmail(mEtSearchByEmail.getText().toString().trim())) {
                        String email = mEtSearchByEmail.getText().toString().trim();
                        getUserDetailsFromFirebaseDB(email);
                    } else {
                        Toast.makeText(this, "Please enter a valid email id to search", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Please enter email id to search", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tvInvite:
                invite();
                break;
            case R.id.tvSelectMember:
                mMemberDetailsLayout.setVisibility(View.GONE);
                boolean presentInList = false;
                if (mMemberEmail != null && mMemberName != null && mMemberUserId != null) {

                    if (mMemberList.size() == 0) {
                        if (mMemberEmail.equals(SharedPreferenceManager.
                                getInstance(ApplicationLoader.getInstance()).getString(Constants.KEY_EMAIL))){
                            Toast.makeText(this, "User Already exists ", Toast.LENGTH_SHORT).show();
                        }else {
                            mUserModel = new User();
                            mtvNoMemberText.setVisibility(View.GONE);
                            mMemberRecyclerView.setVisibility(View.VISIBLE);
                            mUserModel.setEmail(mMemberEmail);
                            mUserModel.setUserId(mMemberUserId);
                            mUserModel.setUsername(mMemberName);
                            mMemberList.add(mUserModel);
                            mAdater.setDataInAdapter(mMemberList);
                            mSaveLayout.setVisibility(View.VISIBLE);
                        }
                    } else {
                        for (int i = 0; i < mMemberList.size(); i++) {
                            if (mMemberList.get(i).getEmail().equals(mMemberEmail)||
                                    mMemberEmail.equals(SharedPreferenceManager.
                                            getInstance(ApplicationLoader.getInstance()).getString(Constants.KEY_EMAIL))) {
                                presentInList = true;

                                Toast.makeText(this, "User already exists", Toast.LENGTH_SHORT).show();
                            }
                        }

                        if (!presentInList) {
                            mUserModel = new User();
                            mtvNoMemberText.setVisibility(View.GONE);
                            mMemberRecyclerView.setVisibility(View.VISIBLE);
                            mUserModel.setEmail(mMemberEmail);
                            mUserModel.setUserId(mMemberUserId);
                            mUserModel.setUsername(mMemberName);
                            mMemberList.add(mUserModel);
                            mAdater.setDataInAdapter(mMemberList);
                            mSaveLayout.setVisibility(View.VISIBLE);
                            presentInList = false;
                        }
                    }


                }
                break;
            case R.id.tvYes:
                if (mMemberEmail != null && mMemberName != null && mMemberUserId != null) {
                    //save group data
                    SaveGroupDetails.writeGroupDetails(UUID.randomUUID().toString(),
                            mMemberList, mGroupName, mGroupDescription, null);
                    Intent intent = new Intent(this, HomeActivity.class);
                    intent.putExtra(Constants.FLAG, Constants.ADD_MEMBER__ACTIVITY_FLAG);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                break;
            case R.id.tvNo:
                Intent intent = new Intent(this, HomeActivity.class);
                intent.putExtra(Constants.FLAG, Constants.ADD_MEMBER__ACTIVITY_FLAG);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
    }

    private boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void getUserDetailsFromFirebaseDB(String email) {
        Firebase users = new Firebase(FIREBASE_URL).child("users");
        users.orderByChild("email").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() == null) {
                    mMemberDetailsLayout.setVisibility(View.GONE);
                    minviteLayout.setVisibility(View.VISIBLE);
                } else {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        try {
                            Object object = child.getValue();
                            Gson gson = new Gson();
                            String json = gson.toJson(object);

                            JSONObject jsonObject = new JSONObject(json);
                            mMemberDetailsLayout.setVisibility(View.VISIBLE);
                            minviteLayout.setVisibility(View.GONE);

                            mMemberName = jsonObject.getString("username");
                            mMemberEmail = jsonObject.getString("email");
                            mMemberUserId = child.getKey();

                            mtvSearchName.setText(jsonObject.getString("username"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

                Log.d("User", firebaseError.getMessage());
            }
        });
    }


    private void invite() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        String shareBody = SharedPreferenceManager.getInstance(ApplicationLoader.getInstance()).
                getString(Constants.KEY_NAME) + "\nInvites you to download the app PhotoShare !";

        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.subject_here));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_via)));

    }
}
